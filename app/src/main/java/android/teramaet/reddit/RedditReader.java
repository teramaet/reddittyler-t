package android.teramaet.reddit;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;

public class RedditReader extends SingleFragmentActivity implements ActivityCallback {

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        mediaPlayer = MediaPlayer.create(RedditReader.this, R.raw.boop);
    }

    MediaPlayer mediaPlayer;
    @LayoutRes
    @Override
    protected int getLayoutResId() {
        return R.layout.activity_masterdetail;
    }

    @Override
    protected Fragment createFragment() {
        mediaPlayer = MediaPlayer.create(RedditReader.this, R.raw.boop);
        return new RedditListFragment();
    }

    @Override
    public void onPostSelected(Uri redditPostUri) {

        if(findViewById(R.id.detail_fragment_container) == null) {
            mediaPlayer.start();
            Intent intent = new Intent(getApplicationContext(), RedditWebActivity.class);
            intent.setData(redditPostUri);
            startActivity(intent);
        }
        else{
            mediaPlayer.start();
            Fragment detailFragment = RedditWebFragment.newFragment(redditPostUri.toString());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detail_fragment_container, detailFragment)
                    .commit();
        }
    }
}
